import 'package:flutter/material.dart';

ThemeData appTheme(BuildContext context) {
  return ThemeData(
      primaryColor: Color.fromARGB(255, 255, 60, 0),
      primaryColorDark: Color.fromARGB(255, 201, 55, 0),
      visualDensity: VisualDensity.adaptivePlatformDensity,
      primaryIconTheme: Theme.of(context).primaryIconTheme.copyWith(color: Colors.white),
      primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white))
  );
}
