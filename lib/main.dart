import 'package:flutter/material.dart';
import 'package:subreddit_app/screen/HomeScreen.dart';
import 'package:subreddit_app/theme/style.dart';

void main() {
  runApp(SubRedditApp());
}

class SubRedditApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SubReddit App',
      theme: appTheme(context),
      home: HomeScreen(),
    );
  }
}