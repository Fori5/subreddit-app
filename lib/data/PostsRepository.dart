import 'package:dio/dio.dart';
import 'package:subreddit_app/db/DatabaseHelper.dart';
import 'package:subreddit_app/model/Post.dart';

final String apiUrl = "https://www.reddit.com/r/FlutterDev/hot/.json";

Future<List<Post>> getPosts() async {
  var fetched = await _fetchPosts();
  if (fetched != null && fetched.isNotEmpty) {
    _storePosts(fetched);
    return fetched;
  } else {
    return _loadPostsFromDb();
  }
}

void _storePosts(List<Post> posts) async {
  var db = DatabaseHelper.instance;
  var rows = posts
      .map((e) => {
            DatabaseHelper.columnTitle: e.title,
            DatabaseHelper.columnMessage: e.message
          })
      .toList();
  await db.deleteAll();
  await db.insert(rows);
}

Future<List<Post>> _loadPostsFromDb() async {
  var db = DatabaseHelper.instance;
  var rows = await db.queryAllRows();
  if(rows == null || rows.isEmpty) {
    return null;
  }
  return rows.map((e) => Post(e[DatabaseHelper.columnTitle], e[DatabaseHelper.columnMessage])).toList();
}

Future<List<Post>> _fetchPosts() async {
  print("fetchPosts");
  try {
    Response<Map> response = await Dio().get(apiUrl);
    return (response.data["data"]["children"] as List)
        .sublist(0, 20)
        .map((i) => Post.fromJson(i))
        .toList();
  } catch (e) {
    print(e);
    return null;
  }
}
