import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:subreddit_app/data/PostsRepository.dart';
import 'package:subreddit_app/model/Post.dart';
import 'package:subreddit_app/screen/DetailScreen.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Post> _posts = [];

  @override
  void initState() {
    super.initState();
    _fetchRedditPosts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SubReddit App"),
      ),
      body: Container(
        child: _buildList(),
      ),
    );
  }

  Widget _buildList() {
    return _posts.length != 0
        ? RefreshIndicator(
            child: ListView.builder(
                itemCount: _posts.length,
                itemBuilder: (BuildContext context, int index) {
                  return buildPost(index);
                }),
            onRefresh: _fetchRedditPosts,
          )
        : Center(child: CircularProgressIndicator());
  }

  Widget buildPost(int index) {
    var message = _posts[index].message;
    return Card(
      child: InkWell(
        child: ListTile(
          title: Text(_posts[index].title),
          subtitle: Text(message.substring(0, min(message.length, 100))),
      ),
      onTap: () => _onItemClick(index),
    ));
  }

  void _onItemClick(int index) {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => DetailScreen(post: _posts[index]))
    );
  }

  Future<void> _fetchRedditPosts() async {
    var posts = await getPosts();
    print("has posts (${posts.length})");
    if(posts == null) {
      _showErrorDialog();
      return;
    }
    setState(() {
      _posts = posts;
    });
  }

  void _showErrorDialog() async {
    bool retry =  await showDialog(context: context,
    builder: (_) => AlertDialog(
      title: Text("Error"),
      content: Text("Error occurred during posts fetch."),
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context, true),
          child: Text('RETRY'),
        ),
        FlatButton(
          onPressed: () => Navigator.pop(context, false),
          child: Text('EXIT'),
        )
      ],
    ));

    if(retry) {
      setState(() {
        _posts = [];
      });
      _fetchRedditPosts();
    } else {
      exit(0);
    }
  }
}
