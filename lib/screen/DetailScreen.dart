import 'package:flutter/material.dart';
import 'package:subreddit_app/model/Post.dart';

class DetailScreen extends StatelessWidget {
  final Post post;

  DetailScreen({Key key, @required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(post.title),
      ),
      body: Container(
          child: ListView(
        padding: EdgeInsets.all(16.0),
        children: [
          Container(
            child: Text(
              post.title,
              style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
            ),
            margin: EdgeInsets.only(bottom: 16.0),
          ),
          Text(post.message)
        ],
      )),
    );
  }
}
