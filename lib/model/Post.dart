class Post {
  final String title;
  final String message;

  Post(this.title, this.message);

  Post.fromJson(Map<String, dynamic> json)
      : title = json['data']['title'],
        message = json['data']['selftext'];

}
